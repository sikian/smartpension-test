# Smart Pension - DevOps test

#### Sebastian Neira Farriol (sikian@gmail.com)

**Disclaimer 1:** In order to make this document as approachable as possible, I've assumed that the reader has never used k8s but has heard about it and possibly used other similar tools.

**Disclaimer 2:** From what I've gathered, there was no need for a CI/CD pipeline -- if I'm mistaken on this point please let me know and I can build one.

In this test I'll be deploying an HTTP server which stores and requests simple data from a Redis database. Traffic will be handled by GKE's internal routing and a simple example of Ingress.

This example has been deployed to a live cluster. Should you want to check it out, please let me know and I'll grant you access to the GCP project.

## The app

I built a simple HTTP server with Koa (Node.js) that will save messages in a list in Redis at the `POST /msg` endpoint.

```sh
$ curl -H "Content-Type: application/json" --data '{ "msg": "message one" }' http://localhost:3000/msg
{ "success": "OK" }
```

This list can then be accessed through `GET /msgs`.

```sh
$ curl http://localhost:3000/msgs
["message one", "message two"]
```

NOTE: I ended using Node.js as I seem to have forgotten a great deal on how to start a simple RoR server. Please let me know if you'd like me to add a RoR container and I'll investigate how to get there!

## Deployment

The manifests for the services are available in `./k8s` (app.yaml, redis.yaml, etc.). Further information what services are and how they're defined can be found [here](https://kubernetes.io/docs/concepts/services-networking/service/), should it help out.

We configure `kubectl` with gcloud's tools:

```sh
$ gcloud container clusters get-credentials $CLUSTER_NAME --region $CLUSTER_REGION
```

And deploy changes as follows:

```sh
$ kubectl apply -f ./k8s/app.yaml -f ./k8s/redis.yaml -f ./k8s/ingress.yaml
```

The app's image has been pushed to GCP's container registry (more info [here](https://cloud.google.com/kubernetes-engine/docs/concepts/ingress)) and I'll be using the official `redis:5` image.

Regarding replication, I haven't set the app's replicas as GKE will be handling them with autoscale. However, as Redis is stateful, we would have to use something akin to Redis Cluster or Redis Sentinel to handle scalability correctly.

### Using Ingress

K8s' Ingress provides a great deal of flexibility when dealing with HTTP and HTTPS, decoupling traffic handling from the services (just as one would expect when using nginx or Traefik). As I've used GKE for this test, I'll be using GKE's ingress controller and set the app's ServiceType to NodePort so it's not accessible from outside the cluster (only through Ingress). More info [here](https://kubernetes.io/docs/concepts/services-networking/ingress/).
