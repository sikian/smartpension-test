module.exports = async ({ request, response }) => {
  const { method, url } = request;
  const { status, message } = response;
  console.log(`${method} ${status} ${url} ${message}`);
};
