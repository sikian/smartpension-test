const Redis = require("ioredis");

const connect = opts => {
  const redis = new Redis(...opts);
  return redis;
};
