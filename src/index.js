const Koa = require("koa");
const Router = require("koa-router");
const Redis = require("ioredis");
const bodyparser = require("koa-bodyparser");

const reqlog = require("./logger");

const app = new Koa();
const router = new Router();
const redis = new Redis({
  host: process.env.REDIS_URL || "172.17.0.3",
  connectTimeout: 10000,
  maxRetriesPerRequest: 2
});

app.use(bodyparser());

router.get("/", async (ctx, next) => {
  ctx.response.body = "Welcome to the test!";
  await next();
});

router.get("/msgs", async (ctx, next) => {
  const list = await redis.lrange("msg", "0", "-1");
  ctx.response.body = list;
  await next();
});

const secureBody = a => a; // secure the body however it seems fit
router.post("/msg", async (ctx, next) => {
  const { msg } = secureBody(ctx.request.body);
  await redis.rpush("msg", msg);
  ctx.response.body = { success: "OK" };
  await next();
});

app.use(router.routes()).use(router.allowedMethods());

// Logs request
app.use(reqlog);

// HTTP server startup
const { PORT = 3000 } = process.env;
console.log(`Starting http server in :${PORT}`);
app.listen(PORT);
